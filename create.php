<?php
include_once("view/layout/header.php");
?>

<div class="container">
    <div class="d-flex pt-4">
		<h3 class="text">Product Add</h3>
		<a href="index.php" class="btn btn-info float-right ml-auto">Cancel</a>
        
        
	</div>
    <hr>
    
	<div id="msg"></div>
	<div class="row">
        <div class="col-md-12">           
            <form action="add.php" method="post" name="form1" >
                <div class="form-group">
                    <label for="sku">SKU</label>
                    <input type="text" name="sku" class="form-control w-25" id="sku" required>
                </div>
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" name="name" class="form-control w-25" id="name" required>
                </div>
                <div class="form-group">
                    <label for="price">Price($)</label>
                    <input type="text" name="price" class="form-control w-25" id="price" required>
                </div>
                <div class="form-group">
                    <label for="typeSwitcher">Type Switcher</label>

                    <select class="form-control w-25" id="typeSwitcher">
                        <option selected disabled value=''></option>
                        <option value="disc">DVD-disc</option>
                        <option value="book">Book</option>
                        <option value="furniture">Furniture</option>
                    </select>
                </div>
                <div class="form-group selectable" name="disc" id="disc" style="display: none;">
                    <label for="size">Size(MB)</label>
                    <input type="text" name="size" class="form-control w-25" id="size">
                    <small>Please provide size in MB format.</small>
                </div>
                <div class="form-group selectable" name="book" id="book" style="display: none;">
                    <label for="weight">Weight(KG)</label>
                    <input type="text" name="weight" class="form-control w-25" id="weight">
                    <small>Please provide weight in KG format.</small>
                </div>
                <div class="form-group selectable" name="furniture" id="furniture" style="display: none;">
                    <label for="height">Height(CM)</label>
                    <input type="text" name="height" class="form-control w-25" id="height">
                    <label for="width">Width(CM)</label>
                    <input type="text" name="width" class="form-control w-25" id="width">
                    <label for="length">Length(CM)</label>
                    <input type="text" name="length" class="form-control w-25" id="length">
                    <small>Please provide dimensions in HxWxL format.</small>
                </div>
                <div class="form-group">
                    <input class="btn btn-success" type="submit" name="Submit" value="Save" onClick="header('Location:index.php');">
                </div>
            </form>
        </div>
    </div>
</div>

<?php
	include_once("view/layout/footer.php");
?>

<script>
    $("#typeSwitcher").on("change", function() {
        $(".selectable").hide();
        const a = $("#" + $(this).val()).show();
        console.log(a);
    })

	function validate() {
		if (document.form1.sku.value == '') {
			alert('Please provide unique sku');
			document.form1.name.focus();				
			return false;
		}
		if (document.form1.name.value == '') {
			alert('Please provide a name');
			document.form1.name.focus();
			return false;
		}
        if (document.form1.price.value == '') {
			alert('Please provide a price');
			document.form1.price.focus();				
			return false;
		}
		return true;
	}
</script>	
<?php
//database connection file
include_once("classes/Crud.php");

$crud = new Crud();

//fetching data in descending order (lastest entry first)
$query = "SELECT * FROM products ORDER BY id DESC";
$result = $crud->getData($query);


if(isset($_POST['submit']))
{

	if(isset($_POST['id']))
	{
	  	foreach($_POST['id'] as $checkbox)
		{
  
			$query1 = 'DELETE FROM products WHERE id='.$checkbox;
			$result1 = $crud->execute($query1);
		    if ($result1) 
		    {
	            header("Location:index.php");
            }
	    }
	}
   
}



include_once("view/layout/header.php");
?>  
    <form action="index.php" method="post">
		<div class="container">
			<div class="d-flex pt-3">
				<h3 class="text">Product List</h3>
				<a href="create.php" class="btn btn-info float-right ml-auto">ADD</a>
				<input type="submit" name="submit" class="delete-checkbox" value="MASS DELETE" onclick="return confirm"> 
			</div>
			<hr>
			<div class="row">
				<?php 
					foreach ($result as $key => $res) {		
					echo "<div class='col-md-3 mt-3'>";
					echo "<div class='card text-center bg-light'>
						<div class='card-body'>";
					echo "<td><input type='checkbox' name='id[]' value='".$res['id']."'></td>";	   
					echo "<p class='card-text'>".$res['sku']."</p>";
					echo "<p>".$res['name']."</p>";
					echo "<p>".$res['price']." $ </p>";
					if ($res['size'] !== "") {
						echo "<p> Size: ".$res['size']." MB </p>";
					} else if ($res['weight'] !== "") {
						echo "<p> Weight: ".$res['weight']." KG </p>";
					} else {
						echo "<p> Dimension: ".$res['dimension']."</p>";
					}
					echo "</div></div>";				
					echo "</div>";		
				}
				?>
			</div>
		</div>
	</form>	
</body>
</html>
<?php
	include_once("view/layout/footer.php");
?>